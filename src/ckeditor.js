/**
 * @license Copyright (c) 2003-2021, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license
 */

// The editor creator to use.
import ClassicEditorBase from "@ckeditor/ckeditor5-editor-classic/src/classiceditor";
import DecoupledEditorBase from "@ckeditor/ckeditor5-editor-decoupled/src/decouplededitor";

import Essentials from "@ckeditor/ckeditor5-essentials/src/essentials";
import GeneralHtmlSupport from "@ckeditor/ckeditor5-html-support/src/generalhtmlsupport";
import UploadAdapter from "@ckeditor/ckeditor5-adapter-ckfinder/src/uploadadapter";
import Autoformat from "@ckeditor/ckeditor5-autoformat/src/autoformat";
import Bold from "@ckeditor/ckeditor5-basic-styles/src/bold";
import Italic from "@ckeditor/ckeditor5-basic-styles/src/italic";
import Underline from "@ckeditor/ckeditor5-basic-styles/src/underline";
import BlockQuote from "@ckeditor/ckeditor5-block-quote/src/blockquote";
// import CKFinder from "@ckeditor/ckeditor5-ckfinder/src/ckfinder";
// import EasyImage from "@ckeditor/ckeditor5-easy-image/src/easyimage";
import Heading from "@ckeditor/ckeditor5-heading/src/heading";
import Indent from "@ckeditor/ckeditor5-indent/src/indent";
import IndentBlock from "@ckeditor/ckeditor5-indent/src/indentblock";
import Link from "@ckeditor/ckeditor5-link/src/link";
import List from "@ckeditor/ckeditor5-list/src/list";
import Paragraph from "@ckeditor/ckeditor5-paragraph/src/paragraph";
import PasteFromOffice from "@ckeditor/ckeditor5-paste-from-office/src/pastefromoffice";
import Table from "@ckeditor/ckeditor5-table/src/table";
import TableToolbar from "@ckeditor/ckeditor5-table/src/tabletoolbar";
import TableProperties from "@ckeditor/ckeditor5-table/src/tableproperties";
import TableSelection from "@ckeditor/ckeditor5-table/src/tableselection";
import TableCellProperties from "@ckeditor/ckeditor5-table/src/tablecellproperties";
import TableUtils from "@ckeditor/ckeditor5-table/src/tableutils";
import TableClipboard from "@ckeditor/ckeditor5-table/src/tableclipboard";
import TextTransformation from "@ckeditor/ckeditor5-typing/src/texttransformation";
import Font from "@ckeditor/ckeditor5-font/src/font";
import Alignment from "@ckeditor/ckeditor5-alignment/src/alignment";
// import Pagination from "@ckeditor/ckeditor5-pagination/src/pagination";
import Mention from "@ckeditor/ckeditor5-mention/src/mention";
import { MentionCustomization } from "./helpers";

class ClassicEditor extends ClassicEditorBase {}
class DecoupledEditor extends DecoupledEditorBase {}

const plugins = [
  Essentials,
  GeneralHtmlSupport,
  UploadAdapter,
  Autoformat,
  Bold,
  Italic,
  BlockQuote,
  Heading,
  Indent,
  IndentBlock,
  Link,
  List,
  Paragraph,
  PasteFromOffice,
  Table,
  TableToolbar,
  TableProperties,
  TableSelection,
  TableCellProperties,
  TableUtils,
  TableClipboard,
  TextTransformation,
  Underline,
  Font,
  Alignment,
  Mention,
  MentionCustomization,
];

const rgbColorConfig = [
  {
    color: "rgb(0, 0, 0)",
    label: "Black",
  },
  {
    color: "rgb(77, 77, 77)",
    label: "Dim grey",
  },
  {
    color: "rgb(153, 153, 153)",
    label: "Grey",
  },
  {
    color: "rgb(230, 230, 230)",
    label: "Light grey",
  },
  {
    color: "rgb(255, 255, 255)",
    label: "White",
    hasBorder: !0,
  },
  {
    color: "rgb(230, 76, 76)",
    label: "Red",
  },
  {
    color: "rgb(230, 153, 76)",
    label: "Orange",
  },
  {
    color: "rgb(230, 230, 76)",
    label: "Yellow",
  },
  {
    color: "rgb(153, 230, 76)",
    label: "Light green",
  },
  {
    color: "rgb(76, 230, 76)",
    label: "Green",
  },
  {
    color: "rgb(76, 230, 153)",
    label: "Aquamarine",
  },
  {
    color: "rgb(76, 230, 230)",
    label: "Turquoise",
  },
  {
    color: "rgb(76, 153, 230)",
    label: "Light blue",
  },
  {
    color: "rgb(76, 76, 230)",
    label: "Blue",
  },
  {
    color: "rgb(153, 76, 230)",
    label: "Purple",
  },
];

const config = {
  htmlSupport: {
    allow: [
      {
        name: /.*/,
        attributes: true,
        classes: true,
        styles: true,
      },
    ],
  },
  fontSize: {
    options: [8, 9, 10, 11, 12, 14, 16, 18, 20, 22, 24, 26, 28, 36, 48, 72],
    supportAllValues: true,
  },
  toolbar: {
    items: [
      "heading",
      "|",
      "bold",
      "italic",
      "underline",
      "|",
      "link",
      "bulletedList",
      "numberedList",
      "|",
      "outdent",
      "indent",
      "|",
      // "uploadImage",
      "blockQuote",
      "insertTable",
      // "mediaEmbed",
      "|",
      "fontSize",
      "fontColor",
      "fontBackgroundColor",
      "alignment",
      "|",
      "undo",
      "redo",
    ],
  },
  table: {
    contentToolbar: [
      "tableColumn",
      "tableRow",
      "mergeTableCells",
      "tableProperties",
      "tableCellProperties",
    ],
    tableProperties: {
      borderColors: rgbColorConfig,
      backgroundColors: rgbColorConfig,
    },
    tableCellProperties: {
      backgroundColors: rgbColorConfig,
      borderColors: rgbColorConfig,
      defaultProperties: {},
    },
  },
  fontBackgroundColor: {
    colors: rgbColorConfig,
  },
  fontColor: {
    colors: rgbColorConfig,
  },
  heading: {
    options: [
      {
        model: "plainText",
        view: "span",
        title: "Plain Text",
        class: "ck-heading_plainText",
      },
      { model: "paragraph", title: "Paragraph", class: "ck-heading_paragraph" },
      {
        model: "heading1",
        view: "h1",
        title: "Heading 1",
        class: "ck-heading_heading1",
      },
      {
        model: "heading2",
        view: "h2",
        title: "Heading 2",
        class: "ck-heading_heading2",
      },
      {
        model: "heading3",
        view: "h3",
        title: "Heading 3",
        class: "ck-heading_heading3",
      },
    ],
  },
  // This value must be kept in sync with the language defined in webpack.config.js.
  language: "en",
};

// Plugins to include in the build.
ClassicEditor.builtinPlugins = plugins;
DecoupledEditor.builtinPlugins = [...plugins];

// Editor configuration.
ClassicEditor.defaultConfig = config;
DecoupledEditor.defaultConfig = {
  ...config,
  toolbar: {
    items: ["|", ...config.toolbar.items],
  },
};

export default {
  ClassicEditor,
  DecoupledEditor,
};
